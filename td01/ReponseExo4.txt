Non car on n'a pas instancié les paramètres.
Démonstration:

var Professeur = Object.create(Personne); // On cr´ee la r´ef´erence `a l’objet Personne
Professeur.initProfesseur = function(nom, prenom, dateNaissance, email) {
  // On appelle la fonction de l’objet Personne
  this.init(nom, prenom, dateNaissance);
  this.email = email;
}

à la ligne 5 on ne dit pas que l'on veut la taille donc ça ne fonctionnera pas.
